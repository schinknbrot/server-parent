package test;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import model.phone.Phone;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.jboss.logging.Logger;

import utils.SessionFactoryUtil;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class TestPhone {

    private static final Logger LOGGER =
            Logger.getLogger(TestPhone.class);

    public static void main(String[] args) throws JsonParseException, JsonMappingException,
            IOException {

        Session session =
                SessionFactoryUtil.getSessionFactory("model/phone").openSession();

        createPhone(session);
        //
        // queryPhones(session);

    }

    private static void queryPhones(Session session) {

        session.beginTransaction();

        Query query =
                session.createQuery("from Phone");

        @SuppressWarnings("unchecked")
        List<Phone> list =
                query.list();

        java.util.Iterator<Phone> iter =
                list.iterator();
        while (iter.hasNext()) {

            Phone phone =
                    iter.next();
            LOGGER.info("GOT: "
                    + phone.getId() + " _ " + phone.getAndroid());
        }

        session.getTransaction().commit();

    }

    private static void createPhone(Session session) {

        session.beginTransaction();
        try {
            System.out.println("Create Phones");
            for (Phone phone : createPhones()) {
                LOGGER.info("SAVE: "
                        + phone.getId());
                LOGGER.info("Return: "
                        + session.save(phone));
            }
            session.getTransaction().commit();
        } catch (HibernateException | IOException e) {
            e.printStackTrace();
        }
    }

    public static List<Phone> createPhones() throws JsonParseException, JsonMappingException,
            IOException {

        Path dir =
                Paths.get("target/classes/phones.json");

        Phone[] phones =
                new ObjectMapper().readValue(dir.toFile(), Phone[].class);

        List<Phone> list =
                new ArrayList<>(Arrays.asList(phones));

        return list;
    }
}
