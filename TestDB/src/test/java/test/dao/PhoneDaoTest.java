package test.dao;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import model.phone.Phone;
import model.phone.dao.PhoneDao;

import org.hibernate.HibernateException;
import org.jboss.logging.Logger;
import org.junit.Before;
import org.junit.Test;

import utils.PhoneCreator;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;

import dao.PhoneDaoImpl;

public class PhoneDaoTest {

    private static final Logger LOGGER =
            Logger.getLogger(PhoneDaoTest.class);

    private static PhoneDao phoneDao =
            null;

    @Before
    public void setUp() {
        phoneDao =
                new PhoneDaoImpl();
    }

    @Test
    public void testQueryAll() {
        for (Phone phone : phoneDao.getAllModels()) {
            LOGGER.info(phone);
        }
    }

    // @Test
    public void testQuery() {
        LOGGER.info(phoneDao.getModelById("droid-2-global-by-motorola"));
    }

    @Test
    public void insertBulk() throws JsonParseException, JsonMappingException, IOException {

        List<Phone> list =
                new ArrayList<>(PhoneCreator.convertJsonToListOfPhones());

        for (Phone phone : list) {
            try {
                phoneDao.insertModel(phone);
            } catch (HibernateException e) {
                LOGGER.warn(e.getLocalizedMessage(), e);
            }
        }
    }

    @Test
    public void testDelete() {
        String id =
                "motorola-charm-with-motoblur";
        Phone deletedPhone =
                phoneDao.deleteModel(id);

        LOGGER.info(phoneDao.getModelById(id));

        phoneDao.insertModel(deletedPhone);

        LOGGER.info(phoneDao.getModelById(id));
    }

}
