package test.dao;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import model.phone.Android;
import model.phone.dao.AndroidDao;

import org.hibernate.HibernateException;
import org.jboss.logging.Logger;
import org.junit.Before;
import org.junit.Test;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import dao.AndroidDaoImpl;

public class AndroidDaoTest {
    private static final Logger LOGGER =
            Logger.getLogger(AndroidDaoTest.class);

    private static AndroidDao androidDao =
            null;

    @Before
    public void setUp() {
        androidDao =
                new AndroidDaoImpl();
    }

    // @Test
    public void testInsert() {
        Android android =
                new Android();
        android.setOs("Android 5.0.2");
        android.setUi("CyanogenMod 12.1");

        androidDao.insertModel(android);
    }

    @Test
    public void getAndroids() throws JsonParseException, JsonMappingException, IOException {
        Path dir =
                Paths.get("target/classes/androids.json");

        Android[] phones =
                new ObjectMapper().readValue(dir.toFile(), Android[].class);

        List<Android> list =
                new ArrayList<>(Arrays.asList(phones));

        for (Android android : list) {
            try {
                androidDao.insertModel(android);
            } catch (HibernateException e) {
                LOGGER.warn(e.getLocalizedMessage(), e);
            }
        }

    }
}
