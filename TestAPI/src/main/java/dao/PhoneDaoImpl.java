package dao;

import java.util.List;

import model.phone.Android;
import model.phone.Battery;
import model.phone.Camera;
import model.phone.Connectivity;
import model.phone.Display;
import model.phone.Hardware;
import model.phone.Phone;
import model.phone.SizeAndWeight;
import model.phone.Storage;
import model.phone.dao.PhoneDao;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.jboss.logging.Logger;

import utils.SessionFactoryUtil;

public class PhoneDaoImpl extends PhoneDao {

    private static final Logger LOGGER =
            Logger.getLogger(PhoneDaoImpl.class);

    private static final SessionFactory SESSION_FACTORY =
            SessionFactoryUtil.getSessionFactory(String.class, Phone.class, Android.class,
                    Battery.class, Camera.class, Display.class, Connectivity.class, Hardware.class,
                    SizeAndWeight.class, Storage.class);

    @Override
    public List<Phone> getAllModels() {

        Session session =
                SESSION_FACTORY.openSession();

        session.beginTransaction();

        Query query =
                session.createQuery("from Phone");

        @SuppressWarnings("unchecked")
        List<Phone> list =
                query.list();

        session.getTransaction().commit();

        return list;
    }

    @Override
    public Phone getModelById(String id) throws HibernateException {
        Session session =
                SESSION_FACTORY.openSession();

        session.beginTransaction();
        LOGGER.info("Looking for Phone with: "
                + id);

        Query query =
                session.createQuery("from Phone where id = :id").setString("id", id);

        Phone phone =
                null;

        @SuppressWarnings("unchecked")
        List<Phone> list =
                query.list();
        if (list.size() == 1) {
            phone =
                    list.get(0);
        }

        session.getTransaction().commit();
        session.disconnect();

        return phone;
    }

    @Override
    public Phone insertModel(Phone model) throws HibernateException {
        Session session =
                SESSION_FACTORY.openSession();

        session.beginTransaction();

        session.merge(model);

        session.getTransaction().commit();
        session.disconnect();

        LOGGER.info("Persisted: "
                + model);
        return model;
    }

    @Override
    public Phone updateModel(Phone model) {
        return null;
    }

    @Override
    public Phone deleteModel(String id) {

        return null;
    }

}
