package dao;

import java.util.List;

import model.phone.Android;
import model.phone.dao.AndroidDao;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.jboss.logging.Logger;

import utils.SessionFactoryUtil;

public class AndroidDaoImpl extends AndroidDao {

    private static final Logger LOGGER =
            Logger.getLogger(AndroidDaoImpl.class);

    private static final SessionFactory SESSION_FACTORY =
            SessionFactoryUtil.getSessionFactory(Android.class);

    @Override
    public List<Android> getAllModels() {
        Session session =
                SESSION_FACTORY.openSession();

        session.beginTransaction();

        @SuppressWarnings("unchecked")
        List<Android> list =
                session.createQuery("FROM Android").list();

        session.getTransaction().commit();
        session.disconnect();
        return list;
    }

    @Override
    public Android insertModel(Android model) throws HibernateException {
        Session session =
                SESSION_FACTORY.openSession();

        session.beginTransaction();

        session.persist(model);

        session.getTransaction().commit();
        session.disconnect();

        LOGGER.info("Persisted: "
                + model);

        return model;
    }

    @Override
    public Android updateModel(Android model) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Android deleteModel(String id) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Android getModelById(String id) {
        // TODO Auto-generated method stub
        return null;
    }

}
