package utils;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import model.phone.Android;
import model.phone.Phone;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class PhoneCreator {

    public static void main(String[] args) {
        try {
            System.out.println(convertJsonToListOfPhones());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static List<Phone> convertJsonToListOfPhones() throws JsonParseException,
            JsonMappingException, IOException {
        Path dir =
                Paths.get("target/classes/phones_complete.json");

        Phone[] phones =
                new ObjectMapper().readValue(dir.toFile(), Phone[].class);

        List<Phone> list =
                new ArrayList<>(Arrays.asList(phones));

        return list;
    }

    public static List<Phone> getListOfPhones() {
        List<Phone> list =
                new ArrayList<>();

        list.add(new Phone()
                .withId("motorola-xoom-with-wi-fi")
                .withImages(
                        new ArrayList<>(Arrays.asList("img/phones/motorola-xoom-with-wi-fi.0.jpg",
                                "img/phones/motorola-xoom-with-wi-fi.1.jpg",
                                "img/phones/motorola-xoom-with-wi-fi.2.jpg",
                                "img/phones/motorola-xoom-with-wi-fi.3.jpg",
                                "img/phones/motorola-xoom-with-wi-fi.4.jpg",
                                "img/phones/motorola-xoom-with-wi-fi.5.jpg")))
                .withAge(1)
                .withImageUrl("img/phones/motorola-xoom-with-wi-fi.0.jpg")
                .withName("Motorola XOOM™ with Wi-Fi")
                .withSnippet(
                        "The Next, Next Generation\r\n\r\nExperience the future with Motorola XOOM with Wi-Fi, "
                                + "the world's first tablet powered by Android 3.0 (Honeycomb).")
                .withAdditionalFeatures("Sensors: proximity, ambient light, barometer, gyroscope")
                .withDescription(
                        "Motorola XOOM with Wi-Fi has a super-powerful dual-core processor and "
                                + "Android™ 3.0 (Honeycomb) — "
                                + "the Android platform designed specifically for tablets. "
                                + "With its 10.1-inch HD widescreen display, "
                                + "you’ll enjoy HD video in a thin, light, powerful and "
                                + "upgradeable tablet.")
                .withAndroid(new Android().withOs("Android 3.0").withUi("Honeycomb")));
        return list;
    }

}
