package model;

public abstract class AbstractModel {

    private String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id =
                id;
    }

    public AbstractModel() {
    }

    public AbstractModel(String id) {
        this.id =
                id;
    }
}
