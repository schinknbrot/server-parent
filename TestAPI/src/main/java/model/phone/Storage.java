package model.phone;

import javax.annotation.Generated;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import org.apache.commons.lang3.builder.ToStringBuilder;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Generated("org.jsonschema2pojo")
@Entity
public class Storage {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    private String flash;
    private String ram;

    @JsonIgnore
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "id")
    private Phone phone;

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    public long getId() {

        return id;

    }

    public void setId(long id) {
        this.id =
                id;
    }

    /**
     * Sets the specified value for id and returns this.
     */
    public Storage withId(long id) {
        this.id =
                id;
        return this;
    }

    public String getFlash() {

        return flash;

    }

    public void setFlash(String flash) {
        this.flash =
                flash;
    }

    /**
     * Sets the specified value for flash and returns this.
     */
    public Storage withFlash(String flash) {
        this.flash =
                flash;
        return this;
    }

    public String getRam() {

        return ram;

    }

    public void setRam(String ram) {
        this.ram =
                ram;
    }

    /**
     * Sets the specified value for ram and returns this.
     */
    public Storage withRam(String ram) {
        this.ram =
                ram;
        return this;
    }

    public Phone getPhone() {

        return phone;

    }

    public void setPhone(Phone phone) {
        this.phone =
                phone;
    }

    /**
     * Sets the specified value for phone and returns this.
     */

    public Storage withPhone(Phone phone) {
        this.phone =
                phone;
        return this;
    }
}
