package model.phone;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import org.apache.commons.lang3.builder.ToStringBuilder;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class Battery {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    private String standbyTime;
    private String talkTime;
    private String type;

    @JsonIgnore
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "id")
    private Phone phone;

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id =
                id;
    }

    /**
     * Sets the specified value for id and returns this.
     */
    public Battery withId(long id) {
        this.id =
                id;
        return this;
    }

    public String getStandbyTime() {
        return standbyTime;
    }

    public void setStandbyTime(String standbyTime) {
        this.standbyTime =
                standbyTime;
    }

    /**
     * Sets the specified value for standbyTime and returns this.
     */
    public Battery withStandbyTime(String standbyTime) {
        this.standbyTime =
                standbyTime;
        return this;
    }

    public String getTalkTime() {
        return talkTime;
    }

    public void setTalkTime(String talkTime) {
        this.talkTime =
                talkTime;
    }

    /**
     * Sets the specified value for talkTime and returns this.
     */
    public Battery withTalkTime(String talkTime) {
        this.talkTime =
                talkTime;
        return this;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type =
                type;
    }

    /**
     * Sets the specified value for type and returns this.
     */
    public Battery withType(String type) {
        this.type =
                type;
        return this;
    }

    public Phone getPhone() {
        return phone;
    }

    public void setPhone(Phone phone) {
        this.phone =
                phone;
    }

    /**
     * Sets the specified value for phone and returns this.
     */
    public Battery withPhone(Phone phone) {
        this.phone =
                phone;
        return this;
    }
}
