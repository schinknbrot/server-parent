package model.phone;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Generated;
import javax.persistence.CascadeType;
import javax.persistence.CollectionTable;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import org.apache.commons.lang3.builder.ToStringBuilder;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Generated("org.jsonschema2pojo")
@Entity
public class SizeAndWeight {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @ElementCollection
    @CollectionTable(name = "SizeAndWeight_DIMENSIONS", joinColumns = @JoinColumn(name = "ID"))
    private List<String> dimensions =
            new ArrayList<String>();
    private String weight;

    @JsonIgnore
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "id")
    private Phone phone;

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id =
                id;
    }

    /**
     * Sets the specified value for id and returns this.
     */
    public SizeAndWeight withId(long id) {
        this.id =
                id;
        return this;
    }

    public List<String> getDimensions() {
        return dimensions;
    }

    public void setDimensions(List<String> dimensions) {
        this.dimensions =
                dimensions;
    }

    /**
     * Sets the specified value for dimensions and returns this.
     */
    public SizeAndWeight withDimensions(List<String> dimensions) {
        this.dimensions =
                dimensions;
        return this;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight =
                weight;
    }

    /**
     * Sets the specified value for weight and returns this.
     */
    public SizeAndWeight withWeight(String weight) {
        this.weight =
                weight;
        return this;
    }

    public Phone getPhone() {
        return phone;
    }

    public void setPhone(Phone phone) {
        this.phone =
                phone;
    }

    /**
     * Sets the specified value for phone and returns this.
     */
    public SizeAndWeight withPhone(Phone phone) {
        this.phone =
                phone;
        return this;
    }
}
