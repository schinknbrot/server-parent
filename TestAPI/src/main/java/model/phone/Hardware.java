package model.phone;

import javax.annotation.Generated;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import org.apache.commons.lang3.builder.ToStringBuilder;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Generated("org.jsonschema2pojo")
@Entity
public class Hardware {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    private boolean accelerometer;
    private String audioJack;
    private String cpu;
    private boolean fmRadio;
    private boolean physicalKeyboard;
    private String usb;

    @JsonIgnore
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "id")
    private Phone phone;

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    public long getId() {

        return id;

    }

    public void setId(long id) {
        this.id =
                id;
    }

    /**
     * Sets the specified value for id and returns this.
     */
    public Hardware withId(long id) {
        this.id =
                id;
        return this;
    }

    public boolean getAccelerometer() {

        return accelerometer;

    }

    public void setAccelerometer(boolean accelerometer) {
        this.accelerometer =
                accelerometer;
    }

    /**
     * Sets the specified value for accelerometer and returns this.
     */
    public Hardware withAccelerometer(boolean accelerometer) {
        this.accelerometer =
                accelerometer;
        return this;
    }

    public String getAudioJack() {

        return audioJack;

    }

    public void setAudioJack(String audioJack) {
        this.audioJack =
                audioJack;
    }

    /**
     * Sets the specified value for audioJack and returns this.
     */
    public Hardware withAudioJack(String audioJack) {
        this.audioJack =
                audioJack;
        return this;
    }

    public String getCpu() {

        return cpu;

    }

    public void setCpu(String cpu) {
        this.cpu =
                cpu;
    }

    /**
     * Sets the specified value for cpu and returns this.
     */
    public Hardware withCpu(String cpu) {
        this.cpu =
                cpu;
        return this;
    }

    public boolean getFmRadio() {

        return fmRadio;

    }

    public void setFmRadio(boolean fmRadio) {
        this.fmRadio =
                fmRadio;
    }

    /**
     * Sets the specified value for fmRadio and returns this.
     */
    public Hardware withFmRadio(boolean fmRadio) {
        this.fmRadio =
                fmRadio;
        return this;
    }

    public boolean getPhysicalKeyboard() {

        return physicalKeyboard;

    }

    public void setPhysicalKeyboard(boolean physicalKeyboard) {
        this.physicalKeyboard =
                physicalKeyboard;
    }

    /**
     * Sets the specified value for physicalKeyboard and returns this.
     */
    public Hardware withPhysicalKeyboard(boolean physicalKeyboard) {
        this.physicalKeyboard =
                physicalKeyboard;
        return this;
    }

    public String getUsb() {

        return usb;

    }

    public void setUsb(String usb) {
        this.usb =
                usb;
    }

    /**
     * Sets the specified value for usb and returns this.
     */
    public Hardware withUsb(String usb) {
        this.usb =
                usb;
        return this;
    }

    public Phone getPhone() {

        return phone;

    }

    public void setPhone(Phone phone) {
        this.phone =
                phone;
    }

    /**
     * Sets the specified value for phone and returns this.
     */
    public Hardware withPhone(Phone phone) {
        this.phone =
                phone;
        return this;
    }
}
