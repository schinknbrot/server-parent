package model;

public class Contact extends AbstractModel {

    private String preName;

    private String name;

    public Contact() {
        super(null);
    }

    public Contact(String id, String preName, String name) {
        super(id);
        this.preName =
                preName;
        this.name =
                name;
    }

    public String getPreName() {
        return preName;
    }

    public void setPreName(String preName) {
        this.preName =
                preName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name =
                name;
    }

}
