package main.controller;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

import model.AbstractModel;

import org.apache.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;

public abstract class AbstractController<T> {

    private static final Logger LOGGER =
            Logger.getLogger(AbstractController.class);
    protected Map<String, T> modelDB =
            new HashMap<>();
    protected AtomicInteger counter =
            new AtomicInteger();

    /**
     * Adds the {@link AbstractModel} that is posted to server in JsonFormat to
     * the Cache.
     */
    @RequestMapping(method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    public abstract T createModel(@RequestBody final T model);

    /**
     * Adds the {@link AbstractModel} that is posted to server in JsonFormat to
     * the Cache.
     */
    @RequestMapping(method = RequestMethod.POST, value = "/list")
    @ResponseStatus(HttpStatus.CREATED)
    public List<T> createModel(@RequestBody final T[] modelArr) {
        List<T> liste =
                new ArrayList<>();
        for (T model : modelArr) {
            liste.add(createModel(model));
        }
        return liste;
    }

    /**
     * Gets the {@link AbstractModel} with the specified id.
     */
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public T getModelById(@PathVariable final String id) {
        LOGGER.info("#getModelById "
                + id);
        return modelDB.get(id);
    }

    /**
     * Returns all {@link AbstractModel}s from the DB.
     * 
     * @return
     */
    @RequestMapping(method = RequestMethod.GET)
    public Collection<T> getAllModels() {
        LOGGER.info("#getAllModels");
        return modelDB.values();
    }

    /**
     * {@link #updateModel(Integer, AbstractModel)}
     */
    @RequestMapping(value = "/list", method = RequestMethod.PUT)
    public abstract List<T> updateModel(@RequestBody final T[] modelArr);

    /**
     * {@link #updateModel(Integer, AbstractModel)}
     */
    @RequestMapping(method = RequestMethod.PUT)
    public abstract T updateModel(@RequestBody final T model);

    /**
     * Updates the customer in the DataBase.<br>
     * To do so, either the id of the customer or the pathVariable is taken. If
     * both of them are "0", the update process fails.
     * 
     * @param product
     * @return
     */
    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public abstract T updateModel(@PathVariable final String id, @RequestBody final T model);

    /**
     * Removes the specified {@link AbstractModel} from the DB.
     * 
     * @param id
     * @return
     */
    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public T deleteModel(@PathVariable final String id) {
        LOGGER.info("#deleteModel "
                + id);
        return modelDB.remove(id);
    }

    /**
     * Removes the specified {@link AbstractModel} from the DB.
     * 
     * @param model
     * @return
     */
    @RequestMapping(value = "/", method = RequestMethod.DELETE)
    public abstract T deleteModel(@PathVariable final T model);
}
