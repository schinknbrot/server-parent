package main.controller;

import model.JobOffer;

import org.apache.log4j.Logger;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/jobOffer")
public abstract class JobOfferController extends AbstractController<JobOffer> {
    private static final Logger LOGGER =
            Logger.getLogger(JobOfferController.class);

    @Override
    public JobOffer updateModel(@PathVariable String id, @RequestBody JobOffer model) {
        LOGGER.info("#updateModel");
        LOGGER.warn("NOT YET IMPLEMENTED!");
        return null;
    }

}
