package main.controller;

import model.Contact;

import org.apache.log4j.Logger;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/contact")
public abstract class ContactController extends AbstractController<Contact> {

    private static final Logger LOGGER =
            Logger.getLogger(ContactController.class);

    @Override
    public Contact updateModel(@PathVariable String id, @RequestBody Contact model) {
        LOGGER.info("#updateModel");
        LOGGER.warn("NOT YET IMPLEMENTED!");
        return null;
    }

}
