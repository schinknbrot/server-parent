package test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Iterator;
import java.util.List;

import model.phone.Phone;

import org.junit.Test;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.ObjectMapper;

public class PhoneTest {
    private static final String URL =
            "http://localhost:8089/phones";

    private static final String URL_LIST =
            "http://localhost:8089/phones/list";

    @Test
    public void test() {
        Phone phone =
                new Phone();

        RestTemplate restTemplate =
                new RestTemplate();

        System.out.println(restTemplate.postForObject(URL, phone, Phone.class));
    }

    @Test
    public void concatJSONFiles() throws IOException {
        Path dir =
                Paths.get("C:\\Users\\julius\\git\\angular-phonecat\\app\\phones\\");
        Iterator<Path> files =
                Files.walk(dir).iterator();

        List<String> linesPhone =
                null;

        StringBuilder strBuilder =
                new StringBuilder("[");

        while (files.hasNext()) {
            Path file =
                    files.next();
            if (file.toString().toLowerCase().endsWith(".json")
                    && !file.toString().toLowerCase().endsWith("phones.json")) {
                List<String> lines =
                        Files.readAllLines(file);
                for (String string : lines) {
                    strBuilder.append(string);
                }
                strBuilder.append(",");

            } else if (file.toString().toLowerCase().endsWith("phones.json")) {
                linesPhone =
                        Files.readAllLines(file);
            }
        }
        strBuilder.deleteCharAt(strBuilder.lastIndexOf(","));
        strBuilder.append("]");

        StringBuilder strBuilderPhones =
                new StringBuilder();

        for (String string : linesPhone) {
            strBuilderPhones.append(string);
        }

        System.out.println(strBuilderPhones);
        System.out.println();
        System.out.println(strBuilder);

        Phone[] phones =
                new ObjectMapper().readValue(strBuilderPhones.toString(), Phone[].class);

        Phone[] phonesDetails =
                new ObjectMapper().readValue(strBuilder.toString(), Phone[].class);

        RestTemplate restTemplate =
                new RestTemplate();

        System.out.println(restTemplate.postForObject(URL_LIST, phones, Phone[].class));

        restTemplate.put(URL_LIST, phonesDetails);
    }
}
