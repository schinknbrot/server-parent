package test;

import java.util.Collection;

import model.Contact;
import model.JobOffer;

import org.apache.log4j.Logger;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.web.client.RestTemplate;

public class BasicTest {

    private static final Logger LOGGER =
            Logger.getLogger(BasicTest.class);

    private static final String URL_JOB_OFFER =
            "http://localhost:8089/jobOffer";

    private static final String URL_CONTACT =
            "http://localhost:8089/contact";

    @Test
    public void testConnectionJobOffer() {

        JobOffer offer =
                new JobOffer("REF-123", "Title", "Desc", "Entu", "HHuber");
        RestTemplate restTemplate =
                new RestTemplate();

        LOGGER.info(restTemplate.postForObject(URL_JOB_OFFER, offer, JobOffer.class));
        LOGGER.info(restTemplate.postForObject(URL_JOB_OFFER, offer, JobOffer.class));

        LOGGER.info(restTemplate.getForObject(URL_JOB_OFFER, Collection.class));
        Assert.assertEquals(1, restTemplate.getForObject(URL_JOB_OFFER, Collection.class).size());

    }

    @Test
    public void testConnectionContact() {

        Contact contact =
                new Contact("HHuber", "Hans", "Huber");
        RestTemplate restTemplate =
                new RestTemplate();

        LOGGER.info(restTemplate.postForObject(URL_CONTACT, contact, Contact.class));
        LOGGER.info(restTemplate.postForObject(URL_CONTACT, contact, Contact.class));

        LOGGER.info(restTemplate.getForObject(URL_CONTACT, Collection.class));
        Assert.assertEquals(1, restTemplate.getForObject(URL_CONTACT, Collection.class).size());

    }
}
